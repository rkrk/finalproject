# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150423161449) do

  create_table "cars", force: :cascade do |t|
    t.string   "vin"
    t.string   "make"
    t.string   "model"
    t.string   "color"
    t.decimal  "wholeSaleCost", precision: 10, scale: 2
    t.decimal  "price",         precision: 10, scale: 2
    t.boolean  "isSold"
    t.decimal  "mileage"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.decimal  "tax",           precision: 10, scale: 2
    t.integer  "year"
  end

  create_table "customers", force: :cascade do |t|
    t.string   "firstName"
    t.string   "lastName"
    t.string   "phone"
    t.string   "classification"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "employees", force: :cascade do |t|
    t.string   "firstName"
    t.string   "lastName"
    t.string   "phone"
    t.string   "position"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "quotes", force: :cascade do |t|
    t.integer  "customer_id"
    t.integer  "employee_id"
    t.integer  "car_id"
    t.decimal  "totalPrice",   precision: 10, scale: 2
    t.boolean  "isFinal"
    t.integer  "duration"
    t.decimal  "interestRate"
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
  end

end
