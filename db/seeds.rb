Car.create!([
  {vin: "1GCDT19W11K645266", make: "BMW", model: "330", color: "Silver", wholeSaleCost: "38000.0", price: "43597.4", isSold: true, mileage: "1.0", tax: "1797.4", year: 2015},
  {vin: "JYAR2348797674633", make: "Ford", model: "Mustang GT", color: "White", wholeSaleCost: "29999.0", price: "34417.85", isSold: false, mileage: "1.0", tax: "1418.95", year: 2015},
  {vin: "1GCDT19W11K645266", make: "Lexus", model: "IS 250", color: "Metallic Gray", wholeSaleCost: "41000.0", price: "47039.3", isSold: false, mileage: "5.0", tax: "1939.3", year: 2015},
  {vin: "1FAHP56U65A051967", make: "Dodge", model: "RAM 1500", color: "Black", wholeSaleCost: "28000.0", price: "32124.4", isSold: false, mileage: "1.0", tax: "1324.4", year: 2015},
  {vin: "YS3ED48E413982562", make: "Lexus", model: "IS F", color: "Red", wholeSaleCost: "61000.0", price: "69985.3", isSold: true, mileage: "1.0", tax: "2885.3", year: 2015},
  {vin: "WDBRF61J33A748366", make: "Chevy", model: "Silverado ", color: "White", wholeSaleCost: "27000.0", price: "30977.1", isSold: false, mileage: "25.0", tax: "1277.1", year: 2015},
  {vin: "SAJWA01AX6F495409", make: "Audi", model: "R8", color: "Silver", wholeSaleCost: "109000.0", price: "125055.7", isSold: false, mileage: "0.0", tax: "5155.7", year: 2015},
  {vin: "1GTJ6LF9XB8293582", make: "Mercedes Binz", model: "CLA 350", color: "Red", wholeSaleCost: "43000.0", price: "49333.9", isSold: true, mileage: "1.0", tax: "2033.9", year: 2015},
  {vin: "1FTVX14584N105307", make: "Volkswagen", model: "Jetta", color: "Black", wholeSaleCost: "18000.0", price: "20651.4", isSold: false, mileage: "5.0", tax: "851.4", year: 2015},
  {vin: "2FTRX08L92C341598", make: "Porsche", model: "911 GT", color: "White", wholeSaleCost: "125000.0", price: "143412.5", isSold: true, mileage: "0.0", tax: "5912.5", year: 2015},
  {vin: "JHGAB73913JN43256", make: "Ford", model: "Mustang", color: "White", wholeSaleCost: "29999.99", price: "34418.99", isSold: false, mileage: "1.0", tax: "1419.0", year: 2015}
])
Customer.create!([
  {firstName: "Basil", lastName: "Chehayeb", phone: "7134449243", classification: "Active"},
  {firstName: "Jon", lastName: "Cogan", phone: "6543210387", classification: "Active"},
  {firstName: "Michele", lastName: "Rivas", phone: "6543287492", classification: "Active"},
  {firstName: "Jeff", lastName: "Ogden", phone: "7139876201", classification: "Active"},
  {firstName: "Meghan", lastName: "Murlock", phone: "8326520198", classification: "Active"},
  {firstName: "Tuong", lastName: "Pham", phone: "2815548697", classification: "Active"}
])
Employee.create!([
  {firstName: "Chris", lastName: "Pickens", phone: "8321119287", position: "Sales Person"},
  {firstName: "Tongtran", lastName: "Vuong", phone: "9879876544", position: "Inactive"},
  {firstName: "Keith", lastName: "Lancaster", phone: "7134449874", position: "Dealership Owner"},
  {firstName: "Huy", lastName: "Nguyen", phone: "4136587981", position: "Inventory Manager"},
  {firstName: "Ben", lastName: "Moyette", phone: "6521327094", position: "Finance Manager"},
  {firstName: "Kyle", lastName: "Sackett", phone: "7136985412", position: "Sales Person"}
])
Quote.create!([
  {customer_id: 2, employee_id: 1, car_id: 1, totalPrice: "43597.4", isFinal: true, duration: 0, interestRate: "0.0"},
  {customer_id: 4, employee_id: 1, car_id: 2, totalPrice: "41154.12", isFinal: false, duration: 3, interestRate: "12.0"},
  {customer_id: 2, employee_id: 1, car_id: 8, totalPrice: "65844.6", isFinal: true, duration: 5, interestRate: "12.0"},
  {customer_id: 1, employee_id: 6, car_id: 3, totalPrice: "47039.3", isFinal: false, duration: 0, interestRate: "0.0"},
  {customer_id: 3, employee_id: 6, car_id: 4, totalPrice: "38372.16", isFinal: false, duration: 4, interestRate: "9.0"},
  {customer_id: 1, employee_id: 1, car_id: 10, totalPrice: "166354.2", isFinal: true, duration: 5, interestRate: "6.0"},
  {customer_id: 3, employee_id: 1, car_id: 11, totalPrice: "34418.99", isFinal: false, duration: 0, interestRate: "0.0"},
  {customer_id: 5, employee_id: 6, car_id: 5, totalPrice: "87338.16", isFinal: true, duration: 3, interestRate: "15.0"}
])
