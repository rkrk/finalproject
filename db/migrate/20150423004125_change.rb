class Change < ActiveRecord::Migration
  def self.up

      change_column :cars, :price, :decimal,:precision => 10, :scale => 2
      change_column :cars, :tax, :decimal,:precision => 10, :scale => 2
      change_column :cars, :wholeSaleCost, :decimal,:precision => 10, :scale => 2
      change_column :quotes, :totalPrice, :decimal,:precision => 10, :scale => 2
      remove_column :quotes, :tax
    end
  def self.down
    change_column :cars, :price, :decimal
    change_column :cars, :tax, :decimal
    change_column :cars, :wholeSaleCost, :decimal
    change_column :quotes, :totalPrice, :decimal
    add_column :quotes, :tax, :decimal
  end
end
