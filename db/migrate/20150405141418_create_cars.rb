class CreateCars < ActiveRecord::Migration
  def change
    create_table :cars do |t|
      t.string :vin
      t.string :make
      t.string :model
      t.string :color
      t.decimal :wholeSaleCost
      t.decimal :price
      t.boolean :isSold
      t.decimal :mileage

      t.timestamps null: false
    end
  end
end
