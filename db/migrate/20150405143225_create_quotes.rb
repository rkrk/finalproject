class CreateQuotes < ActiveRecord::Migration
  def change
    create_table :quotes do |t|
      t.integer :customer_id
      t.integer :employee_id
      t.integer :car_id
      t.decimal :totalPrice
      t.boolean :isFinal
      t.integer :duration
      t.decimal :interestRate

      t.timestamps null: false
    end
  end
end
