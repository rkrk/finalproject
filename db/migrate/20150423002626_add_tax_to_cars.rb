class AddTaxToCars < ActiveRecord::Migration
  def change
    add_column :cars, :tax, :decimal
  end
end
