class CreateCustomers < ActiveRecord::Migration
  def change
    create_table :customers do |t|
      t.string :firstName
      t.string :lastName
      t.string :phone
      t.string :classification

      t.timestamps null: false
    end
  end
end
