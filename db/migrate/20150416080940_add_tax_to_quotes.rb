class AddTaxToQuotes < ActiveRecord::Migration
  def change
    add_column :quotes, :tax, :float
  end
end
