class CreateEmployees < ActiveRecord::Migration
  def change
    create_table :employees do |t|
      t.string :firstName
      t.string :lastName
      t.string :phone
      t.string :position

      t.timestamps null: false
    end
  end
end
