/**
 * Created by partner on 4/15/2015.
 */
    function getInterestRate() {

    //delete rows currently in table
    var letabled = document.getElementById("letable");
    var rowCount = letabled.rows.length;
    for(p = rowCount-1; p > -1; p--)
    {
        letabled.deleteRow(p);
    }


    //date vars
    var currentTime = new Date();
    var month = currentTime.getMonth() + 1;
    var year = currentTime.getFullYear();

    //calculation vars
    var duration = parseInt(document.getElementById("quote_duration").value);
    var intrate = document.getElementById("quote_interestRate").value;
    var markupprice_withtax = parseFloat(document.getElementById("price1").value);

    //var price = document.getElementByID("") need to get marked up price to display for selected car
    var table = document.getElementById("letable");

    var row;
    var cell0;
    var cell1;
    var cell2;
    var cell3;
    var cell4;
    var cell5;
    var stdpay = computeTotal(intrate, duration, markupprice_withtax);
    var balance = markupprice_withtax;
    var paypermonth = stdpay[0];
    var accum_interest = 0;
    var amt_toward_principle = 0;
    //for loop
    for(i = 1; i <= (duration * 12); i++ ) {


        var row = table.insertRow((i-1));
        var cell0 = row.insertCell(0);
        var cell1 = row.insertCell(1);
        var cell2 = row.insertCell(2);
        var cell3 = row.insertCell(3);
        var cell4 = row.insertCell(4);
        var cell5 = row.insertCell(5);
            //payment #
        cell0.innerHTML = i;
        cell1.innerHTML = month + "/01/" + year;


        //interest paid,
        intpayment = balance * (intrate/1200);
        accum_interest = accum_interest + intpayment;
        //principal paid
        amt_toward_principle = paypermonth - intpayment;

        //remaining balance
        balance = balance - amt_toward_principle;


        cell2.innerHTML = "$" +  stdpay[0];
        cell3.innerHTML = "$" + intpayment.toFixed(2);
        cell4.innerHTML = "$" + amt_toward_principle.toFixed(2);
        cell5.innerHTML = "$" + balance.toFixed(2);

        //check last payment

            if(balance<=0){
                cell3.innerHTML = "$" + ((-1*balance) + intpayment).toFixed(2);
                cell5.innerHTML = "$" + 0.00;
            }

            //date stuff
        month++;
        if(month >= 13){
            year = year + 1;
            month = 1;
        }
    }
    if(duration == 0){
        stdpay[1] = markupprice_withtax.toFixed(2);
    }
    document.getElementById("showww").innerHTML = "<h5>Interest Rate: " + intrate +"%<br><br>"
    + "Duration: " + duration + " years<br><br>" + "Base Price: $" + markupprice_withtax.toFixed(2) + "<br><br>Total Price: $" + stdpay[1] + "</h5>" ;

    $("#quote_totalPrice").val(stdpay[1]);
}

function computeTotal(interestc, durationc, markuppricec){
    //get rid of these constants and make sure markupprice has sales tax applied before being passed in

    interestc = interestc/1200;

    durationc = durationc * 12;
    expon = Math.pow((1+interestc), durationc);
    payments =  (markuppricec * interestc * expon)/(expon-1);
    payments = payments.toFixed(2);


    total = payments * durationc;
    total = total.toFixed(2);

    return [payments, total];

}

