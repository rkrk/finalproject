json.array!(@employees) do |employee|
  json.extract! employee, :id, :firstName, :lastName, :phone, :position
  json.url employee_url(employee, format: :json)
end
