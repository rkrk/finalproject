json.array!(@cars) do |car|
  json.extract! car, :id, :vin, :make, :model, :color, :wholeSaleCost, :price, :isSold, :mileage
  json.url car_url(car, format: :json)
end
