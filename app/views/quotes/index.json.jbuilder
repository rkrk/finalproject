json.array!(@quotes) do |quote|
  json.extract! quote, :id, :customer_id, :employee_id, :car_id, :totalPrice, :isFinal, :duration, :interestRate
  json.url quote_url(quote, format: :json)
end
