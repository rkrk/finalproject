module ApplicationHelper
  def sortable(column, title)
    if title == ""
      title = column.titleize
    end
    css_c = column == sort_col ? "current #{sort_dir}" : nil
    direction = column == sort_col && sort_dir == "asc" ? "desc" : "asc"
    link_to title, {:sort => column, :direction => direction}, {:class => css_c}
  end


end
