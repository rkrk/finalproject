class Employee < ActiveRecord::Base
  has_many :quotes
  validates :firstName, :lastName, presence:true
  def full_name
    "#{lastName}, #{firstName}"
  end
end
