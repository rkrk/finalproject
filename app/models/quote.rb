class Quote < ActiveRecord::Base
  after_save :check_sold
  belongs_to :car
  belongs_to :customer
  belongs_to :employee
  validates :totalPrice, :employee_id, :customer_id, presence:true
  validates :totalPrice, :numericality => { greater_than_or_equal_to: 0}
  validates :interestRate, :numericality => { greater_than_or_equal_to: 0, :less_than_or_equal_to => 100}
def check_sold
  a = car_id
  c = customer_id
  if isFinal
    Customer.where(:id => c).update_all(:classification => "Active")
    Car.where(:id => a).update_all(:isSold => true)
  else

    Car.where(:id => a).update_all(:isSold => false)

  end
  def reporting
  @revenue = Quote.where(:isFinal => true).sum("totalPrice").to_s
  @salestax = Car.where(:isSold => true).sum(:tax).to_s
  @soldmsrp = Car.where(:isSold => true).sum(:wholeSaleCost).to_s
  @profit = @revenue - @salestax - @soldmsrp

  end

end

end
