class Customer < ActiveRecord::Base
  has_many :quotes
  validates :firstName, :lastName, presence:true

  def full_name
    "#{lastName}, #{firstName}"
  end
  def self.search(query)
    where("firstName like ? OR lastName like ?", "%#{query}%", "%#{query}%")
  end
end
