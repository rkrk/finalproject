class Car < ActiveRecord::Base
  has_many :quotes
  validates :vin, :make, :model, :color, :wholeSaleCost, :price, :mileage, presence:true
  validates :wholeSaleCost, :price, :mileage, :year,  :numericality => {greater_than_or_equal_to: 0}
  validates :vin, length: { is: 17, message: "The VIN is not 17 characters long"}
  #validates_format_of :price, :with => /^[0-9]+\.[0-9]{2}$/, multiline:true, :message => "price must only contain numbers and contain two decimal places"

  def full_car
    if nil?
      " "
    else
      "#{make} #{model} #{color} #{year} #{vin}"
    end

  end

def self.search(query)
  where("make like ? OR model like ? OR color like?", "%#{query}%", "%#{query}%", "%#{query}%")
end
end


