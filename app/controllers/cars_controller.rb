class CarsController < ApplicationController
  before_action :set_car, only: [:show, :edit, :update, :destroy]

  helper_method :sort_col, :sort_dir

  def update_price

      @cars = Car.where("id" => params[:car_id]).first.price
      if @cars.nil?
        @cars = 0
      end
      render :partial => 'cars/cars', :object => @cars
  end
  # GET /cars
  # GET /cars.json
  def index
    if params[:search]
      @cars = Car.search(params[:search])
      else
    @cars = Car.order(sort_col + " " + sort_dir)
    end
  end

  # GET /cars/1
  # GET /cars/1.json
  def show
  end

  # GET /cars/new
  def new
    @car = Car.new
  end

  # GET /cars/1/edit
  def edit
    @cars = Car.all
  end

  # POST /cars
  # POST /cars.json
  def create
    @car = Car.new(car_params)

    respond_to do |format|
      if @car.save
        format.html { redirect_to @car, notice: 'Car was successfully created.' }
        format.json { render :show, status: :created, location: @car }
      else
        format.html { render :new }
        format.json { render json: @car.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /cars/1
  # PATCH/PUT /cars/1.json
  def update
    respond_to do |format|
      if @car.update(car_params)
        format.html { redirect_to @car, notice: 'Car was successfully updated.' }
        format.json { render :show, status: :ok, location: @car }
      else
        format.html { render :edit }
        format.json { render json: @car.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /cars/1
  # DELETE /cars/1.json
  def destroy
    @car.destroy
    respond_to do |format|
      format.html { redirect_to cars_url, notice: 'Car was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def sort_col
    params[:sort] || "vin"

  end
  def sort_dir
    %w[asc, desc].include?(params[:direction]) ? params[:direction] : "asc"
  end




    # Use callbacks to share common setup or constraints between actions.
    def set_car
      @car = Car.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def car_params
      params.require(:car).permit(:vin, :make, :model, :color, :wholeSaleCost, :price, :isSold, :mileage, :tax, :year)
    end
end
