class QuotesController < ApplicationController
  before_action :set_quote, only: [:show, :edit, :update, :destroy]

  helper_method :sort_col, :sort_dir

  # GET /quotes
  # GET /quotes.json
  def index

    @quotes = Quote.joins('JOIN CARS c ON Quotes.car_id = c.id JOIN CUSTOMERS ON Quotes.customer_id = CUSTOMERS.id JOIN EMPLOYEES ON Quotes.employee_id = EMPLOYEES.id ').order(sort_col + " " + sort_dir)

  end
  def profitt
    @revenue = Quote.where(:isFinal => true).sum("totalPrice").to_f
    @salestax = Car.where(:isSold => true).sum(:tax).to_f
    @soldmsrp = Car.where(:isSold => true).sum(:wholeSaleCost).to_f
    @profitt = 0;
    if((@revenue - @salestax - @soldmsrp) >= 0)
      @profitt = @revenue - @salestax - @soldmsrp
    else
      @profitt = (@revenue - @salestax - @soldmsrp * (-1))
    end

      @profitt

  end
  def revenue
    @revenue = Quote.where(:isFinal => true).sum("totalPrice").to_f
  end

  def taxsale
    @salestax = Car.where(:isSold => true).sum(:tax).to_f
  end
  # GET /quotes/1
  # GET /quotes/1.json
  def show
  end

  # GET /quotes/new
  def new
    @quote = Quote.new
  end

  # GET /quotes/1/edit
  def edit
  end
  # POST /quotes
  # POST /quotes.json
  def create
    @quote = Quote.new(quote_params)

    respond_to do |format|
      if @quote.save
        format.html { redirect_to @quote, notice: 'Quote was successfully created.' }
        format.json { render :show, status: :created, location: @quote }
      else
        format.html { render :new }
        format.json { render json: @quote.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /quotes/1
  # PATCH/PUT /quotes/1.json
  def update
    respond_to do |format|
      if @quote.update(quote_params)
        format.html { redirect_to @quote, notice: 'Quote was successfully updated.' }
        format.json { render :show, status: :ok, location: @quote }

      else
        format.html { render :edit }
        format.json { render json: @quote.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /quotes/1
  # DELETE /quotes/1.json
  def destroy
    @quote.destroy
    respond_to do |format|
      format.html { redirect_to quotes_url, notice: 'Quote was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_quote
      @quote = Quote.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def quote_params
      params.require(:quote).permit(:customer_id, :employee_id, :car_id, :totalPrice, :isFinal, :duration, :interestRate)
    end
  def sort_col
    params[:sort] || "created_at"

  end
  def sort_dir
    %w[desc, asc].include?(params[:direction]) ? params[:direction] : "desc"
  end
end
